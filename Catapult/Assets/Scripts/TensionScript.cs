﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TensionScript : MonoBehaviour
{

    private Rigidbody _rigidbody;
    [SerializeField]private float timer = 0.0f;
    [SerializeField]private float launchForce;
    // Start is called before the first frame update
    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            _rigidbody.AddForce(timer * 100f, 0f, 0f);
            timer += Time.deltaTime;
            launchForce = timer * -10000f;
        }
    }
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            _rigidbody.AddForce(launchForce, 0f, 0f);
            timer = 0f;
            launchForce = 0f;
        }

    }
}
