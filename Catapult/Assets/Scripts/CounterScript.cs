﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterScript : MonoBehaviour
{
    [SerializeField] private Text _scoreText;
    [SerializeField] private int _player1Score = 0;
    private void Start()
    {
        //_player1Score = 0;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player1Hit")
        {
            _player1Score += 1;
            _scoreText.text = "Score: " + _player1Score;
        }
        else if (other.tag == "FirstPlayerHit")
        {
            _player1Score += 1;
            _scoreText.text = "Score: " + _player1Score;
        }
    }
}
