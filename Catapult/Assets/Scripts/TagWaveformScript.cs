﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagWaveformScript : MonoBehaviour
{
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Untagged")
        {
            collision.gameObject.tag = this.gameObject.tag;
        }
    }
}
