﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task2_4Script : MonoBehaviour
{
    private float _value1 = 34.9f, _value2 = 11.6f;

    private void Start()
    {
        Debug.Log(СalculateMedian(_value1, _value2));
    }
    static float СalculateMedian(float first, float second)
    {
        float median = (first + second) /2;

        return median;
    }
}
