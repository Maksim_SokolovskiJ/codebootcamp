﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task2_1Script : MonoBehaviour
{
    //Пришлось отойти от ТЗ, поскольку по заданию не нужно присваивать значение для value2.
    //В это случае код бы не имел смысла. Поэтому я присвоил value2 значение 6

    private int _value1 = 3, _value2 = 6;
    void Start()
    {

        _value1 = Add(_value1, _value2);
        _value1 = Add(_value1, _value2);
        Debug.Log(_value1);
    }

    static int Add(int first, int second)
    {
        int result = first + second;
        return result;
    }

         
}
