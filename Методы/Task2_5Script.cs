﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task2_5Script : MonoBehaviour
{
    private float _value1 = 2, _value2 = 6;

    private void Start()
    {
        Debug.Log(СalculatePower(_value1, _value2));
    }

    static float СalculatePower(float first, float second)
    {
        float result = Mathf.Pow(first, second);
        return result;
    }

}
