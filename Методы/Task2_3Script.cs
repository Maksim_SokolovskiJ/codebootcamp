﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task2_3Script : MonoBehaviour
{
    private float _value1 = 22;
    private void Start()
    {
        Debug.Log(DivideByThree(_value1));
    }

    static float DivideByThree(float dividend)
    {
        float quotient = dividend / 3;
        return quotient;
    }

}
