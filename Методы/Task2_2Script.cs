﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task2_2Script : MonoBehaviour
{
    private string message = "Муравей нашёл лазейку между трубок фермы";

    void Start()
    {
        PrintWarning(message);
    }

    static void PrintWarning(string warning)
    {
        Debug.LogWarning("Зафиксировано предупреждение: " + warning);

    }
}
