﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task5Script : MonoBehaviour
{
    private int _value1, _value2;
    void Start()
    {
        _value1 = 3;
        _value2 = (int)Mathf.Pow(_value1, _value1);
        Debug.Log(_value2);
    }

}
